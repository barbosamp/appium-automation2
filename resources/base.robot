*** Settings ***

Library     AppiumLibrary
Library     libs/extend.py
Library     Process
Resource    helpers.robot

*** Keywords ***
Open Session
    Set Appium Timeout      10
    Open Application                    http://localhost:4723/wd/hub
    ...                                 automationName=UiAutomator2
    ...                                 platformName=Android
    ...                                 deviceName=MiPhone
    # ...                                 remoteAdbHost=host.docker.internal
    # ...                                 adbPort=5037
    ...                                 app=app/twp.apk
    Get Started

Close Session
    Capture Page Screenshot
    Close Application

