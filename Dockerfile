FROM budtmo/docker-android-x86-8.1

COPY ubuntu/sources.list /etc/apt/sources.list
COPY . .
RUN apt-get update -y -q  \
  && apt-get install -y -q --no-install-recommends python3-pip \
  && pip3 install --no-cache-dir \
      setuptools==46.2.0

#RUN  apt-get install -y -q --no-install-recommends vim


RUN pip3 

ENV DEVICE="Samsung Galaxy S10"
ENV APPIUM="true"