*** Settings ***

Resource        ../resources/base.robot

# EXECUTA A KEYWORD ANTES DE CADA TESTCASE
Test Setup      Open Session
# EXECUTA A KEYWORD DEPOIS DE CADA TESTCASE
Test Teardown   Close Session

*** Test Cases ***
Deve selecionar a opção Python
    Go To Radio Buttons

    ${element}=         Set Variable        xpath=//android.widget.RadioButton[contains(@text, 'Python')]

    Click Element                       ${element}
    Wait Until Element Is Visible       ${element}
    Element Attribute Should Match      ${element}       checked     true
