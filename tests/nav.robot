*** Settings ***

Resource        ../resources/base.robot

# EXECUTA A KEYWORD ANTES DE CADA TESTCASE
Test Setup      Open Session
# EXECUTA A KEYWORD DEPOIS DE CADA TESTCASE
Test Teardown   Close Session

*** Variables ***
# TIPO DE VARIÁVEIS
#${NOME}         Helder Fernandes
#@{CARROS}       Civic   Lancer  Chevette    Brasilia
#&{CARRO}        nome=Lancer     modelo=Evolution    ano=2020

${TOOLBAR_TITLE}        id=io.qaninja.android.twp:id/toolbarTitle        

*** Test Cases ***
Deve acessar a tela Dialogs
    Open Nav

    Click Text                          DIALOGS
    Wait Until Element Is Visible       ${TOOLBAR_TITLE}
    Element Text Should Be              ${TOOLBAR_TITLE}       DIALOGS

Deve acessar a tela de formulários
    Open Nav

    Click Text                          FORMS
    Wait Until Element Is Visible       ${TOOLBAR_TITLE}       
    Element Text Should Be              ${TOOLBAR_TITLE}       FORMS

Deve acessar a tela de Vingadores
    Open Nav        

    Click Text                          AVENGERS
    Wait Until Element Is Visible       ${TOOLBAR_TITLE}       
    Element Text Should Be              ${TOOLBAR_TITLE}       AVENGERS


