*** Settings ***

Resource        ../resources/base.robot

# EXECUTA A KEYWORD ANTES DE CADA TESTCASE
Test Setup      Open Session
# EXECUTA A KEYWORD DEPOIS DE CADA TESTCASE
Test Teardown   Close Session

*** Test Cases ***
Deve mover o Hulk para o topo da lista
    Go To Avenger List

    Drag And Drop       io.qaninja.android.twp:id/drag_handle       3       0
    Sleep               5   